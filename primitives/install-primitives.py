#!/usr/bin/env python3

import argparse
import json
import os.path
import subprocess
import sys
from distutils import version
from urllib import parse as url_parse

import frozendict
import pip

# To have both stderr and stdout interleaved together.
sys.stderr = sys.stdout

parser = argparse.ArgumentParser(description="Install primitives from primitive annotations.")
parser.add_argument(
    '-a', '--annotations',
    required='D3M_INTERFACE_VERSION' not in os.environ,
    help="directory with primitive annotations to install"
)
arguments = parser.parse_args()

if arguments.annotations:
    annotations_directory = arguments.annotations
else:
    # "D3M_INTERFACE_VERSION" environment variables comes from the core image.
    if os.environ['D3M_INTERFACE_VERSION'] == 'devel':
        # Use latest stable release for primitive annotations for devel image.
        known_versions = [version.LooseVersion(directory.name[1:]) for directory in os.scandir('primitives') if directory.is_dir() and directory.name.startswith('v')]

        max_version = max(known_versions)

        annotations_directory = os.path.join('primitives', 'v{max_version}'.format(max_version=max_version))

    else:
        annotations_directory = os.path.join('primitives', os.environ['D3M_INTERFACE_VERSION'])

can_use_process_dependency_links = version.LooseVersion(pip.__version__) < version.LooseVersion('19.0.0')


def make_hashable(obj):
    if isinstance(obj, dict):
        return frozendict.frozendict({make_hashable(key): make_hashable(value) for key, value in obj.items()})
    elif isinstance(obj, list):
        return tuple(make_hashable(value) for value in obj)
    else:
        return obj


def get_annotations():
    for dirpath, dirnames, filenames in os.walk(annotations_directory):
        for filename in filenames:
            if filename != 'primitive.json':
                continue

            annotation_path = os.path.join(dirpath, filename)

            print(">>> Installing '{annotation_path}'.".format(annotation_path=annotation_path), flush=True)

            with open(annotation_path, 'r') as annotation_file:
                yield json.load(annotation_file)


def run(args):
    subprocess.run(args, stdout=sys.stdout, stderr=sys.stdout, check=True, encoding='utf8')
    sys.stdout.flush()


already_installed_entries = set()
already_updated_apt = False
for annotation in get_annotations():
    try:
        for installation_entry in annotation['installation']:
            # Optimization. If something is already installed, we can skip it.
            # Many primitives share same dependencies.
            hashable_installation_entry = make_hashable(installation_entry)
            if hashable_installation_entry in already_installed_entries:
                continue
            # We add it immediately and if it fails, we also do not retry it in the devel image.
            already_installed_entries.add(hashable_installation_entry)

            if installation_entry['type'] == 'PIP' and 'package' in installation_entry:
                registry = installation_entry.get('registry', 'https://pypi.python.org/simple')
                print(">>> Installing Python package '{package}=={version}' from '{registry}'.".format(package=installation_entry['package'], version=installation_entry['version'], registry=registry), flush=True)
                args = ['pip3', '--disable-pip-version-check', 'install', '--process-dependency-links', '--upgrade', '--upgrade-strategy', 'only-if-needed', '--index-url', registry, '{package}=={version}'.format(package=installation_entry['package'], version=installation_entry['version'])]
                if not can_use_process_dependency_links:
                    args.remove('--process-dependency-links')
                run(args)

            elif installation_entry['type'] == 'PIP' and 'package_uri' in installation_entry:
                print(">>> Installing Python package '{package_uri}'.".format(package_uri=installation_entry['package_uri']), flush=True)
                # git+git scheme is not supported, and other URIs can be parsed with urlparse.
                parsed_uri = url_parse.urlparse(installation_entry['package_uri'])

                # Not a git URI. Just directly install, without "--editable" argument.
                if not parsed_uri.scheme.startswith('git'):
                    package_uri = url_parse.urlunparse(parsed_uri)

                    args = ['pip3', '--disable-pip-version-check', 'install', '--process-dependency-links', '--upgrade', '--upgrade-strategy', 'only-if-needed', package_uri]
                    if not can_use_process_dependency_links:
                        args.remove('--process-dependency-links')
                    run(args)

                else:
                    # To install with "--editable" argument an "egg" fragment argument has to be provided.
                    # We parse it here and set it to Python path to assure it is unique, if it does not yet
                    # exist. We used to not require it in v2017.12.27 and v2018.1.5 interface versions.
                    if parsed_uri.fragment:
                        parsed_fragment = url_parse.parse_qs(parsed_uri.fragment, strict_parsing=True)
                    else:
                        parsed_fragment = {}

                    if 'egg' not in parsed_fragment:
                        parsed_fragment['egg'] = [annotation['python_path']]

                    parsed_uri = parsed_uri._replace(fragment=url_parse.urlencode(parsed_fragment, doseq=True, safe='/', quote_via=url_parse.quote))

                    package_uri = url_parse.urlunparse(parsed_uri)

                    # We install with "--editable" so that packages can have access to their git repositories.
                    # For example, they might need it to compute installation git commit hash for their metadata.
                    args = ['pip3', '--disable-pip-version-check', 'install', '--process-dependency-links', '--upgrade', '--upgrade-strategy', 'only-if-needed', '--editable', package_uri]
                    if not can_use_process_dependency_links:
                        args.remove('--process-dependency-links')
                    run(args)

            elif installation_entry['type'] == 'UBUNTU':
                print(">>> Installing Ubuntu package '{package}'.".format(package=installation_entry['package']), flush=True)
                if not already_updated_apt:
                    run(['apt-get', 'update', '-q', '-q'])
                    already_updated_apt = True
                run(['apt-get', 'install', '--yes', '--force-yes', '--no-install-recommends', installation_entry['package']])

    except Exception as error:
        if arguments.annotations:
            raise
        elif os.environ['D3M_INTERFACE_VERSION'] == 'devel':
            print(">>> Exception during installation but ignoring because it is a devel image: {error}".format(error=error), flush=True)
        else:
            raise


print(">>> All available primitives:", flush=True)
run(['python3', '-m', 'd3m', 'index', 'search'])
