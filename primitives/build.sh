#!/bin/sh -e

# You can run this script locally to build a local version of the image.
#
# Example how to run it to build the image, from the root of this repository:
#
#   ./primitives/build.sh true primitives ubuntu-artful-python36-v2018.4.18 primitives/ubuntu-artful-python36-v2018.4.18.dockerfile registry.gitlab.com/datadrivendiscovery/images/core:ubuntu-artful-python36-v2018.4.18

SHOULD_REBUILD="$1"
IMAGE_NAME="$2"
IMAGE_TAG="$3"
IMAGE_FILE="$4"
CURRENT_IMAGE_BASE="$5"
CURRENT_IMAGE_BASE_DIGEST="$6" # Optional

# Here we assume that the last part of the tag is version.
D3M_INTERFACE_VERSION=${IMAGE_TAG##*-}
CURRENT_IMAGE_BASE_D3M_INTERFACE_VERSION=${CURRENT_IMAGE_BASE##*-}

# Set this to a reasonable value if running locally and not on CI.
if [ -z "$CI_REGISTRY_IMAGE" ]; then
    CI_REGISTRY_IMAGE="registry.gitlab.com/datadrivendiscovery/images"
fi

# Sanity check.
if [ "$D3M_INTERFACE_VERSION" != "$CURRENT_IMAGE_BASE_D3M_INTERFACE_VERSION" ]; then
  echo "Base image primitive interface version ($CURRENT_IMAGE_BASE_D3M_INTERFACE_VERSION) does not match image tag interface version ($D3M_INTERFACE_VERSION)."
  exit 1
fi

if [ ! -d "$IMAGE_NAME/primitives" ]; then
  git clone "https://gitlab.com/datadrivendiscovery/primitives.git" "$IMAGE_NAME/primitives"
fi

CURRENT_PRIMITIVES_COMMIT_SHA=$(git -C "$IMAGE_NAME/primitives" rev-parse HEAD)

# If image exists it was just pulled on CI. Locally, it might be whichever version you have.
if docker inspect "$CI_REGISTRY_IMAGE/$IMAGE_NAME:$IMAGE_TAG" >/dev/null 2>&1; then
  # Primitives repo commit stored in a label in the image. "org.datadrivendiscovery.primitives-commit"
  # is for backwards compatibility for before we had a public images repository. Private images are
  # layered on top of public ones, so we try in this order.
  PRIMITIVES_COMMIT_SHA=$(docker inspect --format='{{or (index .Config.Labels "org.datadrivendiscovery.private.primitives-commit") (index .Config.Labels "org.datadrivendiscovery.public.primitives-commit") (index .Config.Labels "org.datadrivendiscovery.primitives-commit")}}' "$CI_REGISTRY_IMAGE/$IMAGE_NAME:$IMAGE_TAG")
else
  # The first commit in the repository.
  PRIMITIVES_COMMIT_SHA=$(git -C "$IMAGE_NAME/primitives" rev-list --max-parents=0 HEAD)
fi

echo "Current primitives commit: $CURRENT_PRIMITIVES_COMMIT_SHA"
echo "Primitives commit: $PRIMITIVES_COMMIT_SHA"

# We first store the diff into a variable so that we can support the case when diff fails.
# This happens if repository has been force pushed and old commit hash does not exist anymore.
DIFF="$(git -C "$IMAGE_NAME/primitives" diff --name-only "$PRIMITIVES_COMMIT_SHA" "$CURRENT_PRIMITIVES_COMMIT_SHA" || true)"

# This shows which primitive annotations changed since the commit recorded in the image.
echo "Primitives files changed:"
echo "$DIFF"

if [ "$D3M_INTERFACE_VERSION" = "devel" ]; then
  # Use latest stable release for primitive annotations for devel image.
  D3M_INTERFACE_VERSION="v$(python3 -c "import os; from distutils import version; print(max([version.LooseVersion(directory.name[1:]) for directory in os.scandir('$IMAGE_NAME/primitives') if directory.is_dir() and directory.name.startswith('v')]))")"
fi

# To check if changed files are inside the directory for "D3M_INTERFACE_VERSION", we first cut the list of changed
# files to the length of "D3M_INTERFACE_VERSION" and then compare with fixed strings that any line matches
# "D3M_INTERFACE_VERSION". We do not want to use regex here because then we would have to escape it.
if [ "$SHOULD_REBUILD" = "true" ] || echo "$DIFF" | cut -c 1-"${#D3M_INTERFACE_VERSION}" | grep --fixed-strings --line-regexp --quiet "$D3M_INTERFACE_VERSION"; then
  echo "Building Docker image (custom script)"

  TIMESTAMP="$(date -u +"%Y%m%d-%H%M%S")"

  # CI_COMMIT_SHA and CURRENT_IMAGE_BASE_DIGEST are optional.
  # If not provided they will be simply empty.
  docker build --tag="$CI_REGISTRY_IMAGE/$IMAGE_NAME:$IMAGE_TAG" \
   --label="org.datadrivendiscovery.public.source-commit=$CI_COMMIT_SHA" \
   --label="org.datadrivendiscovery.public.base-digest=$CURRENT_IMAGE_BASE_DIGEST" \
   --label="org.datadrivendiscovery.public.primitives-commit=$CURRENT_PRIMITIVES_COMMIT_SHA" \
   --label="org.datadrivendiscovery.public.timestamp=$TIMESTAMP" \
   --build-arg="org_datadrivendiscovery_public_source_commit=$CI_COMMIT_SHA" \
   --build-arg="org_datadrivendiscovery_public_base_digest=$CURRENT_IMAGE_BASE_DIGEST" \
   --build-arg="org_datadrivendiscovery_public_primitives_commit=$CURRENT_PRIMITIVES_COMMIT_SHA" \
   --build-arg="org_datadrivendiscovery_public_timestamp=$TIMESTAMP" \
   --file="$IMAGE_FILE" "$IMAGE_NAME"

  # Push only if on CI.
  if [ -n "$CI_JOB_TOKEN" ]; then
    TAG_WITH_TIMESTAMP="$CI_REGISTRY_IMAGE/$IMAGE_NAME:$IMAGE_TAG-$TIMESTAMP"
    docker tag "$CI_REGISTRY_IMAGE/$IMAGE_NAME:$IMAGE_TAG" "$TAG_WITH_TIMESTAMP"
    docker push "$CI_REGISTRY_IMAGE/$IMAGE_NAME:$IMAGE_TAG"
    docker push "$TAG_WITH_TIMESTAMP"
    echo "Timestamped image is $TAG_WITH_TIMESTAMP"
  fi
else
  echo "Skipping (custom script)"
fi
