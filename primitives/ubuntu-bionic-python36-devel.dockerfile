FROM registry.gitlab.com/datadrivendiscovery/images/core:ubuntu-bionic-python36-devel

# This assumes "build.sh" cloned the repository.
COPY ./primitives /primitives
COPY ./install-primitives.py /install-primitives.py

# In devel image we install primitives which were made for a stable version.
# They can install a stable version of the core package, so after installing
# all primitives we reinstall the devel version of the core package.
# Furthermore, we patch information about dependencies on the core package
# so that the check that the image has consistent dependencies ("pip3 check")
# has more chance to not fail. Nevertheless, we do not run the check because
# also some other dependencies might be inconsistent.
# We remove all /src/*/build directories as a workaround for the pip bug.
# See: https://github.com/pypa/pip/issues/6521
RUN cd / && \
 python3 /install-primitives.py && \
 rm -rf /primitives /install-primitives.py && \
 pip3 --disable-pip-version-check install --upgrade --upgrade-strategy only-if-needed -e git+https://gitlab.com/datadrivendiscovery/d3m.git@devel#egg=d3m && \
 find /src /usr/local/lib/python3.6/dist-packages/ -name requires.txt -print0 | xargs -r -0 sed -i -E 's/^d3m[<>=]{2}.*$/d3m/g' && \
 find /src /usr/local/lib/python3.6/dist-packages/ -path '*dist-info/METADATA' -print0 | xargs -r -0 sed -i -E 's/^Requires-Dist: d3m .*$/Requires-Dist: d3m/g' && \
 rm -rf /src/*/build && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

ARG org_datadrivendiscovery_public_source_commit
ENV ORG_DATADRIVENDISCOVERY_PUBLIC_SOURCE_COMMIT=$org_datadrivendiscovery_public_source_commit

ARG org_datadrivendiscovery_public_base_digest
ENV ORG_DATADRIVENDISCOVERY_PUBLIC_BASE_DIGEST=$org_datadrivendiscovery_public_base_digest

ARG org_datadrivendiscovery_public_primitives_commit
ENV ORG_DATADRIVENDISCOVERY_PUBLIC_PRIMITIVES_COMMIT=$org_datadrivendiscovery_public_primitives_commit

ARG org_datadrivendiscovery_public_timestamp
ENV ORG_DATADRIVENDISCOVERY_PUBLIC_TIMESTAMP=$org_datadrivendiscovery_public_timestamp
