FROM registry.gitlab.com/datadrivendiscovery/images/base:ubuntu-bionic-python36

ENV GOPATH=/usr/local/go
ENV PATH="${GOPATH}/bin:${PATH}"

# Python.
RUN \
 apt-get update -q -q && \
 apt-get install --yes --force-yes build-essential libcap-dev ffmpeg jq && \
 pip3 install python-prctl==1.7 && \
 pip3 install pycodestyle==2.4.0 && \
 pip3 install git+https://github.com/python/mypy.git@3edf07b92508d7387f8d372b0aca4ee025c0996e && \
 pip3 install deepdiff==3.3.0 && \
 pip3 install pyquery==1.4.0 && \
 pip3 install yattag==1.10.0 && \
 pip3 install deep_dircmp==0.1.0 && \
 pip3 install recommonmark==0.5.0 && \
 pip3 install sphinx==1.8.1 && \
 pip3 install sphinxcontrib-fulltoc==1.2.0 && \
 pip3 install sphinx-autodoc-typehints==1.10.3 && \
 pip3 install virtualenv==16.2.0 && \
 pip3 install asv==0.3.1 && \
 pip3 install yq==2.9.2 && \
 pip3 install grpcio grpcio-tools && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Go.
RUN apt-get update -q -q && \
 apt-get install --yes --force-yes golang-go git curl unzip wget && \
 go get -u github.com/golang/protobuf/proto && \
 go get -u github.com/golang/protobuf/protoc-gen-go && \
 go get -u google.golang.org/grpc && \
 go get github.com/ckaznocha/protoc-gen-lint && \
 curl -OL https://github.com/google/protobuf/releases/download/v3.6.1/protoc-3.6.1-linux-x86_64.zip && \
 unzip protoc-3.6.1-linux-x86_64.zip -d protoc3 && \
 cp -a protoc3/bin/protoc /usr/bin/protoc && \
 mkdir -p /usr/local/include/google && \
 cp -a protoc3/include/google/protobuf /usr/local/include/google && \
 rm -rf protoc-3.6.1-linux-x86_64.zip && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# JavaScript.
RUN wget -O - https://nodejs.org/dist/v10.15.1/node-v10.15.1-linux-x64.tar.xz | tar Jx --strip=1 -C /usr/local --anchored --exclude=node-v10.15.1-linux-x64/CHANGELOG.md --exclude=node-v10.15.1-linux-x64/LICENSE --exclude=node-v10.15.1-linux-x64/README.md && \
 npm install grpc && \
 npm install google-protobuf && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

ARG org_datadrivendiscovery_public_source_commit
ENV ORG_DATADRIVENDISCOVERY_PUBLIC_SOURCE_COMMIT=$org_datadrivendiscovery_public_source_commit

ARG org_datadrivendiscovery_public_base_digest
ENV ORG_DATADRIVENDISCOVERY_PUBLIC_BASE_DIGEST=$org_datadrivendiscovery_public_base_digest
