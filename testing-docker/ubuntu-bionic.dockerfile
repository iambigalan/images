FROM ubuntu:bionic

ENV DEBIAN_FRONTEND=noninteractive

# apt-utils seems missing and warnings are shown, so we install it.
RUN apt-get update -q -q && \
 apt-get install --yes --force-yes apt-utils tzdata locales file sudo && \
 echo 'UTC' > /etc/timezone && \
 rm /etc/localtime && \
 dpkg-reconfigure tzdata && \
 apt-get upgrade --yes --force-yes && \
 rm -f /etc/cron.weekly/fstrim && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ENV LC_ALL=en_US.UTF-8

# Locales and some utilities.
RUN sed -i 's/^# deb-src/deb-src/' /etc/apt/sources.list && \
 apt-get update -q -q && \
 apt-get install --yes --force-yes --no-install-recommends git curl wget tar gzip ca-certificates unzip jq gnupg2 && \
 locale-gen --no-purge en_US.UTF-8 && \
 update-locale LANG=en_US.UTF-8 && \
 echo locales locales/locales_to_be_generated multiselect en_US.UTF-8 UTF-8 | debconf-set-selections && \
 echo locales locales/default_environment_locale select en_US.UTF-8 | debconf-set-selections && \
 dpkg-reconfigure locales && \
 curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash && \
 apt-get install --yes --force-yes git-lfs && \
 git lfs install && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

VOLUME /var/lib/docker

# Docker in Docker.
RUN apt-get update -q -q && \
 apt-get install --yes --force-yes apt-transport-https ca-certificates && \
 curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - && \
 echo "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable" > /etc/apt/sources.list.d/docker.list && \
 apt-get update -q -q && \
 apt-get install --yes --force-yes docker-ce && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

ARG org_datadrivendiscovery_public_source_commit
ENV ORG_DATADRIVENDISCOVERY_PUBLIC_SOURCE_COMMIT=$org_datadrivendiscovery_public_source_commit

ARG org_datadrivendiscovery_public_base_digest
ENV ORG_DATADRIVENDISCOVERY_PUBLIC_BASE_DIGEST=$org_datadrivendiscovery_public_base_digest
