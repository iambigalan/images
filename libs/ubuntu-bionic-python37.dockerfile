FROM registry.gitlab.com/datadrivendiscovery/images/shared:ubuntu-bionic-python37

# Install TensorFlow GPU version, and appropriate versions of Keras, Theano, Pytorch, mxnet.
# We also install other fixed versions of ML libraries. Should be kept in sync with
# "run_validation.py" script in primitives repository.
RUN pip3 install \
 tensorflow-gpu==2.2.0  \
 keras==2.3.1 \
 torch==1.4.0 \
 torchvision==0.5.0 \
 Theano==1.0.4 \
 scikit-learn==0.22.2.post1 \
 numpy==1.18.2 \
 pandas==1.0.3 \
 networkx==2.4 \
 pyarrow==0.16.0 \
 scipy==1.4.1 \
 Pillow==7.1.2 \
 Cython==0.29.16 \
 mxnet==1.6.0 \
 pmdarima==1.6.1 \
 && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

ARG org_datadrivendiscovery_public_source_commit
ENV ORG_DATADRIVENDISCOVERY_PUBLIC_SOURCE_COMMIT=$org_datadrivendiscovery_public_source_commit

ARG org_datadrivendiscovery_public_base_digest
ENV ORG_DATADRIVENDISCOVERY_PUBLIC_BASE_DIGEST=$org_datadrivendiscovery_public_base_digest
