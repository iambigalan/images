A repository with multiple Docker images used in the D3M program.

If you are just starting and do not know which Docker image to use,
use `registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-<latest d3m core package version>`.

## Structure

Currently supported images are:

* `registry.gitlab.com/datadrivendiscovery/images/base:ubuntu-bionic-python36` — Base Docker image with Python 3.6 and general build dependencies.
* `registry.gitlab.com/datadrivendiscovery/images/shared:ubuntu-bionic-python36` — Docker image based on `base`, with CUDA.
* `registry.gitlab.com/datadrivendiscovery/images/libs:ubuntu-bionic-python36` — Docker image based on `shared`, with various ML libraries with fixed versions.
* `registry.gitlab.com/datadrivendiscovery/images/testing:ubuntu-bionic-python36` — Docker image based on `shared`, with Docker-in-Docker, Kubernetes-in-Docker, Go, JavaScript/node.js used for CI testing in other repositories.
* `registry.gitlab.com/datadrivendiscovery/images/core:ubuntu-bionic-python36-<version>` — Docker image based on `libs`, with
  [core D3M Python package](https://gitlab.com/datadrivendiscovery/d3m) installed.
  `<version>` stands for a particular release of that package. There is also an `ubuntu-bionic-python36-devel` Docker
  tag which corresponds to an image which is automatically build from the latest development code in `devel` branch
  of the package.
* `registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-<version>` — Docker image based on `core`, with
  [core D3M Python package](https://gitlab.com/datadrivendiscovery/d3m), and all primitives submitted to
  [open source primitives index](https://gitlab.com/datadrivendiscovery/primitives) installed.
  `<version>` stands for a particular release of the core package and only primitives submitted
  under this version are installed.
* `registry.gitlab.com/datadrivendiscovery/images/libs-lite:ubuntu-bionic-python36-<version>` — A lite version of the `libs` image without Docker-in-Docker, CUDA, and large ML libraries. Based on `base` only.
* `registry.gitlab.com/datadrivendiscovery/images/core-lite:ubuntu-bionic-python36-<version>` — A lite version of the `core` image without Docker-in-Docker, CUDA, and large ML libraries. Based on `libs-lite`.
* `registry.gitlab.com/datadrivendiscovery/images/testing-lite:ubuntu-bionic-python36` — A lite version of the `testing` image without Docker-in-Docker, Kubernetes-in-Docker, and CUDA. Based on `base` only.
* `registry.gitlab.com/datadrivendiscovery/images/testing-docker:ubuntu-bionic` — A small testing image with only Docker-in-Docker, without Python 3.6.

There is also a special image `registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-devel` which
uses primitive annotations for the latest stable release of the core package, but tries to install them into the image with
development versions of the core package. Some primitives might not success in installation and they might be missing from the
final image. But this is a good image to test against future versions and use newest features from the core package.

See the [Docker image UI](https://docs.datadrivendiscovery.org/docker.html) for a list of images and their tags.

`core` and `primitives` images (except for `devel` image) are being build and tagged with a release version of the core package and if you
use one of those tagged images you can be assured that the core package will not change under you.

Despite this though, images still change through time because base images change with security and other similar updates
to system packages and dependencies, or primitives are installed or updated. Those updates should never introduce any
observable changes, but to support complete reproducibility, each built image is also tagged with a timestamp-based
tag which is never overridden with a different image. The tag is of the form
`registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-<version>-<timestamp>`.

For example, instead of using `registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-xenial-python36-v2017.12.27`
identifier, use, for example,
`registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-xenial-python36-v2017.12.27-20180322-083834`.
This identifier points to a concrete version of an image build at one point in time.

## GPU support

Docker images are made in a way that they can run both with or without GPU support. If you want primitives to have GPUs available and use them,
see [mode information here how to run the image](https://github.com/NVIDIA/nvidia-docker). Otherwise, they will run without GPUs.

## License Agreements

By downloading these images, you agree to the terms of the license agreements for NVIDIA software included in the images.
