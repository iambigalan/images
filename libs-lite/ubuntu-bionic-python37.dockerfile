FROM registry.gitlab.com/datadrivendiscovery/images/base:ubuntu-bionic-python37

# We install fixed versions of ML libraries. Should be kept in sync with
# "run_validation.py" script in primitives repository.
RUN pip3 install \
 scikit-learn==0.22.2.post1 \
 numpy==1.18.2 \
 pandas==1.0.3 \
 networkx==2.4 \
 pyarrow==0.16.0 \
 scipy==1.4.1 \
 Pillow==7.1.2 \
 Cython==0.29.16 \
 pmdarima==1.6.1 \
 && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

ARG org_datadrivendiscovery_public_source_commit
ENV ORG_DATADRIVENDISCOVERY_PUBLIC_SOURCE_COMMIT=$org_datadrivendiscovery_public_source_commit

ARG org_datadrivendiscovery_public_base_digest
ENV ORG_DATADRIVENDISCOVERY_PUBLIC_BASE_DIGEST=$org_datadrivendiscovery_public_base_digest
